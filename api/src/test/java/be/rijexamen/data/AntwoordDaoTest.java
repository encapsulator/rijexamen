package be.rijexamen.data;

import be.rijexamen.Application;
import be.rijexamen.common.antwoord.Antwoord;
import be.rijexamen.data.AntwoordDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@DataJpaTest
public class AntwoordDaoTest {

    @Autowired
    private AntwoordDao dao;

    @Test(expected = Exception.class)
    public void test() {
        dao.save(getAnEmptyAntwoord());
    }

    @Test
    public void testCreateAndPersistAntwoord() {
        Antwoord antwoord = dao.save(getFullAntwoord());
        Antwoord retrieved = dao.getOne(antwoord.getId());
        assertEquals(true, retrieved.isJuist());
        assertEquals("antwoord", retrieved.getContent());
    }

    @Test
    public void testUpdateAntwoord() {
        Long rows = dao.count();
        Antwoord antwoord = dao.save(getFullAntwoord());
        Antwoord retrieved = dao.getOne(antwoord.getId());
        retrieved.setContent("new content");
        retrieved.setJuist(false);

        Antwoord update = dao.save(retrieved);
        Antwoord updateRetrieved = dao.getOne(update.getId());
        assertNotNull(updateRetrieved);
        assertEquals("new content", updateRetrieved.getContent());
        assertEquals(false, updateRetrieved.isJuist());
        assertEquals(rows+1, dao.count());


    }



    private Antwoord getAnEmptyAntwoord() {
        return new Antwoord(false, null);
    }

    private Antwoord getFullAntwoord() {
        return new Antwoord(true, "antwoord");
    }

}
