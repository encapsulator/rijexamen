package be.rijexamen.data;

import be.rijexamen.Application;
import be.rijexamen.common.vraag.Vraag;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@DataJpaTest
public class VraagDaoTest {

    @Autowired
    private VraagDao dao;

    @Test
    public void testCreateAndPersistAntwoord() {
        Vraag fullVraag = dao.save(getFullVraag());
        Vraag retrieved = dao.getOne(fullVraag.getId());
        assertEquals("Dit is een vraag", retrieved.getVraag());
    }

    @Test
    public void testUpdateAntwoord() {
        Long rows = dao.count();
        Vraag vraag = dao.save(getFullVraag());
        Vraag retrieved = dao.getOne(vraag.getId());
        retrieved.setVraag("new content");

        Vraag update = dao.save(retrieved);
        Vraag updateRetrieved = dao.getOne(update.getId());
        assertNotNull(updateRetrieved);
        assertEquals("new content", updateRetrieved.getVraag());
        assertEquals(rows+1, dao.count());
    }




    private Vraag getFullVraag() {

        return Vraag.builder().vraag("Dit is een vraag").build();
    }
}