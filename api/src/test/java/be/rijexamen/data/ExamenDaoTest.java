package be.rijexamen.data;

import be.rijexamen.Application;
import be.rijexamen.common.examen.Examen;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@DataJpaTest
public class ExamenDaoTest {
    @Autowired
    private ExamenDao dao;

    @Test
    public void testCreateAndPersistAntwoord() {
        Examen fullExamen = dao.save(getFullExamen());
        Examen retrieved = dao.getOne(fullExamen.getId());
        assertEquals(20, retrieved.getTijdPerVraag());
        assertEquals(2, retrieved.getAantalVragen());
        assertEquals(2, retrieved.getAantalToegelatenFouten());
    }

    @Test
    public void testUpdateAntwoord() {
        Long rows = dao.count();
        Examen examen = dao.save(getFullExamen());
        Examen retrieved = dao.getOne(examen.getId());
        retrieved.setAantalToegelatenFouten(0);
        retrieved.setAantalVragen(1);
        retrieved.setTijdPerVraag(10);

        Examen update = dao.save(retrieved);
        Examen updateRetrieved = dao.getOne(update.getId());
        assertNotNull(updateRetrieved);
        assertEquals(0, updateRetrieved.getAantalToegelatenFouten());
        assertEquals(1, updateRetrieved.getAantalVragen());
        assertEquals(10, updateRetrieved.getTijdPerVraag());
        assertEquals(rows+1, dao.count());
    }




    private Examen getFullExamen() {

        return Examen.builder().aantalVragen(2).puntenPerVraag(10).aantalToegelatenFouten(2).date(new Date(System.currentTimeMillis())).
                tijdPerVraag(20).build();
    }
}