package test.java.be.rijexamen;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = "be.rijexamen")
@PropertySource("test/resources/application-test.properties")
@EnableTransactionManagement
public class H2JpaConfig {
}
