package be.rijexamen.common.antwoord;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import be.rijexamen.common.AbstractDomainEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Antwoord extends AbstractDomainEntity {

    @Column(name = "isJuist")
    private boolean isJuist;

    @Column(name = "content", nullable = false)
    private String content;

}
