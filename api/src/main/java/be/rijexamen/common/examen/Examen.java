package be.rijexamen.common.examen;

import be.rijexamen.common.AbstractDomainEntity;
import be.rijexamen.common.vraag.Vraag;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
@ToString
@EqualsAndHashCode
@Builder
public class Examen extends AbstractDomainEntity {

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "examen_id")
    public List<Vraag> vragen;

    @Basic
    protected Date date = new Date(Calendar.getInstance().getTime().getTime());

    @Column(name = "aantal_vragen")
    private int aantalVragen;

    @Column(name = "tijd_per_vraag")
    private int tijdPerVraag;

    @Column(name = "punten_per_vraag")
    private int puntenPerVraag;

    @Column(name = "aantal_toegelaten_fouten")
    private int aantalToegelatenFouten;

}