package be.rijexamen.common.vraag;

import be.rijexamen.common.AbstractDomainEntity;
import be.rijexamen.common.antwoord.Antwoord;
import be.rijexamen.common.examen.Examen;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Vraag extends AbstractDomainEntity {

    public Vraag(String vraag, Examen examen, Collection<Antwoord> antwoorden) {

        setAntwoorden(antwoorden);
        setVraag(vraag);
    }

    @Column(nullable = false)
    private String vraag;



    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "vraag_id")
    private Collection<Antwoord> antwoorden;


    public void setVraag(String vraag) throws NullPointerException {
        if(vraag == null || vraag.isEmpty()) {
            throw new NullPointerException("QUESTION_NO_STRING");
        }
        this.vraag = vraag;
    }
}
