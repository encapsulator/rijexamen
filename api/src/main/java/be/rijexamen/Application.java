package be.rijexamen;


import be.rijexamen.common.examen.Examen;
import be.rijexamen.data.ExamenDao;
import main.java.be.rijexamen.mapper.JsonToObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@SpringBootApplication
public class Application {

    @Autowired
    private ExamenDao examenDao;



    private static final Logger log = LoggerFactory.getLogger(Application.class);
    private static final boolean RUNDBTEST = false;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args ->  {
            Logger logger = LoggerFactory.getLogger(Application.class);
            logger.info("Start conversion ...");

            File file = this.getFile("vragen_2018.json");

            JsonToObjectMapper examenJsonToObjectMapper = new JsonToObjectMapper();
            Examen examen = null;
            try {
                examen = examenJsonToObjectMapper.convert(file);
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println(examen.getAantalVragen());
            examenDao.save(examen);
        };
    }


    public File getFile(String name) {
        ClassLoader classLoader = this.getClass().getClassLoader();
        System.out.println(this.getClass().getName());
        return new File(classLoader.getResource(name).getFile());

    }
}
