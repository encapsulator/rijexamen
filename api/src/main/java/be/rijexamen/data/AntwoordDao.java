package be.rijexamen.data;

import be.rijexamen.common.antwoord.Antwoord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AntwoordDao extends JpaRepository<Antwoord, Long> {

}
