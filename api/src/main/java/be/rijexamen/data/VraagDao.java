package be.rijexamen.data;

import be.rijexamen.common.vraag.Vraag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VraagDao extends JpaRepository<Vraag, Long> {
}
