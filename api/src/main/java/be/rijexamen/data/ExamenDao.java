package be.rijexamen.data;


import be.rijexamen.common.examen.Examen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExamenDao extends JpaRepository<Examen, Long> {
}
