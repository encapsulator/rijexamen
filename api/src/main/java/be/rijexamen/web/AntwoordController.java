package be.rijexamen.web;

import be.rijexamen.common.antwoord.Antwoord;
import be.rijexamen.data.AntwoordDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("antwoord")
public class AntwoordController {

    @Autowired
    private AntwoordDao antwoordDao;

    @GetMapping("/")
    public List<Antwoord> getAll() {
        return antwoordDao.findAll();
    }

    @GetMapping("/{antwoordId}")
    public Antwoord message(@PathVariable Long antwoordId) {
        return antwoordDao.findById(antwoordId).get();
    }

    @PostMapping("/")
    public Antwoord newAntwoord(@RequestBody Antwoord antwoord) {
        return antwoordDao.save(antwoord);
    }

    @PutMapping("/{antwoordId}")
    public Antwoord changeAntwoord(@RequestBody Antwoord antwoord, @PathVariable Long antwoordID) {
        if(antwoord.getId() == null) antwoord.setId(antwoordID);
        return antwoordDao.save(antwoord);
    }

    @DeleteMapping("/{antwoordId}")
    public void deleteEmployee(@PathVariable Long antwoordId) {
        antwoordDao.deleteById(antwoordId);
    }
}
