package be.rijexamen.web;

import be.rijexamen.common.vraag.Vraag;
import be.rijexamen.data.VraagDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("vraag")
public class VraagController {

    @Autowired
    private VraagDao vraagDao;

    @GetMapping("/")
    public List<Vraag> getAll() {
        return vraagDao.findAll();
    }

    @GetMapping("/{vraagId}")
    public Vraag message(@PathVariable Long vraagId) {
        return vraagDao.findById(vraagId).get();
    }

    @PostMapping("/")
    public Vraag newVraag(@RequestBody Vraag vraag) {
        return vraagDao.save(vraag);
    }

    @PutMapping("/{vraagId}")
    public Vraag changeVraag(@RequestBody Vraag vraag, @PathVariable Long vraagID) {
        if(vraag.getId() == null) vraag.setId(vraagID);
        return vraagDao.save(vraag);
    }

    @DeleteMapping("/{vraagId}")
    public void deleteEmployee(@PathVariable Long vraagId) {
        vraagDao.deleteById(vraagId);
    }

}
