package be.rijexamen.web;

import be.rijexamen.common.examen.Examen;
import be.rijexamen.data.ExamenDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("examen")
public class ExamenController    {

    @Autowired
    private ExamenDao examenDao;

    @GetMapping("/")
    public List<Examen> getAll() {
        return examenDao.findAll();
    }

    @GetMapping("/{examenId}")
    public Examen message(@PathVariable Long examenId) {
        return examenDao.findById(examenId).get();
    }

    @PostMapping("/")
    Examen newExamen(@RequestBody Examen examen) {
        return examenDao.save(examen);
    }

    @PutMapping("/{examenId}")
    public Examen changeExamen(@RequestBody Examen examen, @PathVariable Long examenID) {
        if(examen.getId() == null) examen.setId(examenID);
        return examenDao.save(examen);
    }

    @DeleteMapping("/{examenId}")
    public void deleteEmployee(@PathVariable Long examenId) {
        examenDao.deleteById(examenId);
    }

}
