package main.java.be.rijexamen.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import be.rijexamen.common.examen.Examen;

import java.io.File;

public class JsonToObjectMapper {
    private ObjectMapper mapper = new ObjectMapper();

    public Examen convert(File json) throws Exception {

        return mapper.readValue(json, Examen.class);
    }

}
